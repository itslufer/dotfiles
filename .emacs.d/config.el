;; Size of the garbage collector
(setq gc-cons-threshold (* 50 1000 1000))

;; Enable subprocessing
(setq read-process-output-max (* 1024 1024))

;; Shows start up info
(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(global-set-key (kbd "C-c m") 'recompile)
(global-set-key (kbd "C-c c") 'compile)

(add-to-list 'default-frame-alist
             '(font . "Iosevka-18"))

;; Custom path for themes
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/emacs-theme-gruvbox/")

;; Junio-theme is like chocolate
;; (load-theme 'gruvbox-dark-medium t)

;; Emacs starts on *scratch*
(setq inhibit-startup-message t)
;; No Menu
(menu-bar-mode -1) 
;; No Toolbar
(tool-bar-mode -1)
;; No Scrollbar
(scroll-bar-mode -1)

;; Display numbers
(column-number-mode)

(global-display-line-numbers-mode t)

;; No line highlighting, just like Vim
(setq global-hl-line-modes nil)

;; Line breaks in the end of line
(global-visual-line-mode t)

;; Tab indentation
(setq-default tab-width 4)
(setq-default evil-shift-width tab-width)

;; Use tabs over spaces
(setq-default indent-tabs-mode nil)

;; New lines when cursor continue going down
(setq next-line-add-newlines t)

(normal-erase-is-backspace-mode 0)

;; No double spaces for go to next line
(setq sentence-end-double-space nil)

;; No bells!
(setq ring-bell-function 'ignore)

;; Ask for y/n instead of yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;; When the current file is modified in another place, Emacs will update it automatically
(global-auto-revert-mode)

;; Auto pars
(electric-pair-mode 1)

;; Show matching pars
(show-paren-mode 1)

;; Messages only on the "echo" space
(setq use-dialog-box nil)

;; Resize Emacs by pixel (this avoid some problems with TWMs)
(setq-default frame-resize-pixelwise t)

;; Backups
(setq 
     backup-directory-alist '((".*" . "~/.emacs-files-backup"))
     backup-by-copying t
     version-control t
     delete-old-versions t
     kept-new-versions 20
     kept-old-versions 5
     )

;; No lockfiles  (".#")
(setq create-lockfiles nil)

;; No autosaves
(setq auto-save-default nil)

;; No number lines on some modes
(dolist (mode '(
               org-mode-hook
               term-mode-hook
               shell-mode-hook
               eshell-mode-hook)
               )

(add-hook mode(lambda () (display-line-numbers-mode 0))))
	
(setq default-process-coding-system '(utf-8 . utf-8))

(setq shell-file-name "/bin/bash")

(require 'package)


(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))


(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Share buffers on multiple Emacs windows
;; (server-start)

(use-package which-key
  :ensure t
  :init 
  (which-key-mode))

(require 'org)
(setq org-clock-sound "~/.emacs.d/pomodoro.wav")

(use-package doom-themes
  :ensure t
  :init (load-theme 'doom-one t))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(use-package flycheck
  :ensure t)

(setq c-default-style "linux" 
    c-basic-offset 4)
(use-package cmake-mode
  :ensure t)

(use-package lua-mode
  :ensure t
  :mode ("\\.lua\\'"))
  (setq lua-indent-level 4)

(use-package rust-mode
  :ensure t
  :mode ("\\.rs\\'"))
  (setq rust-indent-level 4)
