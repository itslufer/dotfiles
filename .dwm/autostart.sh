#!/bin/bash

# General

numlockx on &                         # Ativar Num Lock
setxkbmap -option ctrl:nocaps &       # Desativar Caps Look
xset r rate 300 50 &                  # Aumentar a taxa de resposta do teclado
feh --bg-scale ~/.dwm/wallpapers/lua2.jpg &

# Barra de Status

dte(){
	dte="$(date +"%A, %B %d - %H:%M")"
	echo -e "⏳ $dte "
}

mem(){
	mem=`free | awk '/Mem/ {printf "%d MiB/%d Mib\n", $3 / 1024.0, $2 / 1024.0 }'`
	echo -e " ▤ $mem"
}

while true; do
	xsetroot -name "$(mem) | $(dte)"
	# Informações atualizam a cada 10 segundos
	sleep 10s
done &

/usr/bin/emacs --daemon               # Inicia o daemon do Emacs


