let mapleader = " "
set nocompatible

syntax on
set encoding=utf-8

set number
set shiftwidth=2
set tabstop=2
set expandtab
set scrolloff=10
set nowrap

set incsearch
set ignorecase
set smartcase
set showcmd
set showmode
set showmatch

set hlsearch

set background=dark
colorscheme gruvbox

inoremap kq <Esc>
nnoremap <leader>e <Esc>:Lex<CR>
nnoremap <C-w> <Esc>:bd<CR>
nnoremap <C-t> <Esc>:tabnew<CR>
nnoremap <A-l> <Esc>:tabnext<CR>
nnoremap <A-h> <Esc>:tabprevious<CR>

set nobackup
set undodir=~/.vim/backup
set undofile
set undoreload=1000

